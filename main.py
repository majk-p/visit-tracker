import tornado.ioloop
import tornado.web
import redis
import os

from handler import VisitHandler

def make_app():
    return tornado.web.Application([
        (r"/", VisitHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    port = int(os.environ.get("PORT"))
    print("listening on port {}".format(port))
    app.listen(port)
    tornado.ioloop.IOLoop.current().start()
