import tornado.ioloop
import tornado.web
import redis
import os

def increment(cnt):
    """
    >>> increment(None)
    1
    >>> increment("10")
    11
    """
    if cnt is not None:
        return int(cnt, 10)+1
    return 1

class VisitHandler(tornado.web.RequestHandler):

    def get(self):
        url = os.environ.get("REDIS_URL")
        if url is not None:
            r = redis.StrictRedis.from_url(url)
        else:
            r = redis.StrictRedis(host="localhost", port=6379, db=0)
        cnt = r.get('count')
        visit = increment(cnt)
        r.set('count', visit)
        self.write("Visit no. {}".format(visit))

if __name__ == "__main__":
    import doctest
    doctest.testmod()
