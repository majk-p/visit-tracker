FROM python:3.5

EXPOSE 8888

WORKDIR /visitor

COPY main.py main.py
COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt
ENTRYPOINT ./main.py
